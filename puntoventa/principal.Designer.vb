﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CATEGORIAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.COMPRASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VENTASToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VENTAREALIZADAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REALIZARVENTAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EMPLEADOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.USUARIOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INFORMACIONToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EXITENCIASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosToolStripMenuItem, Me.PersonalToolStripMenuItem, Me.VentasToolStripMenuItem, Me.CATEGORIAToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(580, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.COMPRASToolStripMenuItem, Me.VENTASToolStripMenuItem1})
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(75, 20)
        Me.ProductosToolStripMenuItem.Text = "ARTICULO"
        '
        'PersonalToolStripMenuItem
        '
        Me.PersonalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EMPLEADOToolStripMenuItem, Me.USUARIOToolStripMenuItem})
        Me.PersonalToolStripMenuItem.Name = "PersonalToolStripMenuItem"
        Me.PersonalToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.PersonalToolStripMenuItem.Text = "PERSONAL"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.INFORMACIONToolStripMenuItem})
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.VentasToolStripMenuItem.Text = "AYUDA"
        '
        'CATEGORIAToolStripMenuItem
        '
        Me.CATEGORIAToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EXITENCIASToolStripMenuItem})
        Me.CATEGORIAToolStripMenuItem.Name = "CATEGORIAToolStripMenuItem"
        Me.CATEGORIAToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.CATEGORIAToolStripMenuItem.Text = "CATEGORIA"
        '
        'COMPRASToolStripMenuItem
        '
        Me.COMPRASToolStripMenuItem.Name = "COMPRASToolStripMenuItem"
        Me.COMPRASToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.COMPRASToolStripMenuItem.Text = "COMPRAS "
        '
        'VENTASToolStripMenuItem1
        '
        Me.VENTASToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VENTAREALIZADAToolStripMenuItem, Me.REALIZARVENTAToolStripMenuItem})
        Me.VENTASToolStripMenuItem1.Name = "VENTASToolStripMenuItem1"
        Me.VENTASToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.VENTASToolStripMenuItem1.Text = "VENTAS"
        '
        'VENTAREALIZADAToolStripMenuItem
        '
        Me.VENTAREALIZADAToolStripMenuItem.Name = "VENTAREALIZADAToolStripMenuItem"
        Me.VENTAREALIZADAToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.VENTAREALIZADAToolStripMenuItem.Text = "VENTA REALIZADA"
        '
        'REALIZARVENTAToolStripMenuItem
        '
        Me.REALIZARVENTAToolStripMenuItem.Name = "REALIZARVENTAToolStripMenuItem"
        Me.REALIZARVENTAToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.REALIZARVENTAToolStripMenuItem.Text = "REALIZAR VENTA"
        '
        'EMPLEADOToolStripMenuItem
        '
        Me.EMPLEADOToolStripMenuItem.Name = "EMPLEADOToolStripMenuItem"
        Me.EMPLEADOToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EMPLEADOToolStripMenuItem.Text = "EMPLEADO"
        '
        'USUARIOToolStripMenuItem
        '
        Me.USUARIOToolStripMenuItem.Name = "USUARIOToolStripMenuItem"
        Me.USUARIOToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.USUARIOToolStripMenuItem.Text = "USUARIO"
        '
        'INFORMACIONToolStripMenuItem
        '
        Me.INFORMACIONToolStripMenuItem.Name = "INFORMACIONToolStripMenuItem"
        Me.INFORMACIONToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.INFORMACIONToolStripMenuItem.Text = "INFORMACION"
        '
        'EXITENCIASToolStripMenuItem
        '
        Me.EXITENCIASToolStripMenuItem.Name = "EXITENCIASToolStripMenuItem"
        Me.EXITENCIASToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EXITENCIASToolStripMenuItem.Text = "EXITENCIAS"
        '
        'principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 341)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "principal"
        Me.Text = "principal"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents COMPRASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VENTASToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VENTAREALIZADAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REALIZARVENTAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EMPLEADOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents USUARIOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents INFORMACIONToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CATEGORIAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EXITENCIASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
